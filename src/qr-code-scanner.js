import { LitElement, html, css } from "lit";
import { createRef, ref } from "lit/directives/ref.js";
import { BarcodeDetectorPolyfill } from "@undecaf/barcode-detector-polyfill";

try {
	window["BarcodeDetector"].getSupportedFormats();
} catch {
	window["BarcodeDetector"] = BarcodeDetectorPolyfill;
}

/**
 * A QR Code Scanner element
 *
 * @csspart button-start - The button starting the scan
 * @csspart button-stop - The button stopping the scan
 * @csspart video-control - A video element for visual control of the scan
 */
export default class QRCodeScanner extends LitElement {
	static get properties() {
		return {
			/**
			 * Text displayed in the "start scan" button
			 */
			textStart: { type: String, attribute: "text-start" },

			/**
			 * Text displayed in the "stop scan" button
			 */
			textStop: { type: String, attribute: "text-stop" },

			/**
			 * The time interval in milliseconds between each scan attemp
			 */
			interval: { type: Number },

			/**
			 * The list of selected barcode formats to look for when scanning
			 */
			formats: { type: Array },

			/**
			 * The current media stream from a user device camera
			 */
			userMedia: { type: Object },

			/**
			 * The list of media devices available on the device
			 */
			mediaDevices: { type: Array },

			/**
			 * The media constraint to request for available devices on the device
			 */
			constraint: { type: Object },

			/**
			 * Is the scanner open on load, to autoscan on load,
			 * this will request camera permissions when the page loads
			 */
			autoscan: { type: Boolean },

			/**
			 * Is the scanner disabled,
			 * when the device has no support, or user rejected authorizations
			 */
			disabled: { type: Boolean },
		};
	}

	videoRef = createRef();

	constructor() {
		super();
		this.interval = 1000;
		this.formats = ["qr_code"];
		this.textStart = "Scan";
		this.textStop = "Stop";
		this.mediaDevices = [];
		this.constraint = {
			video: { facingMode: "environment" },
			audio: false,
		};
		this.disabled = false;
		this.autoscan = false;
	}

	async connectedCallback() {
		super.connectedCallback();
		if (navigator.mediaDevices.enumerateDevices) {
			const devices = await navigator.mediaDevices.enumerateDevices();
			this.mediaDevices = devices.filter((device) => {
				return device.kind === "videoinput";
			});
		}
		if (this.mediaDevices.length) {
			if (this.autoscan) {
				this.start();
			}
		}
	}

	async disconnectedCallback() {
		super.disconnectedCallback();
		await this.stop();
	}

	render() {
		return html`
			${this.userMedia ? this._renderStarted() : this._renderStopped()}
		`;
	}
	_renderStopped() {
		return html`
			<button
				@click=${() => this.start()}
				part="button-start"
				title="Start scanning"
				?disabled=${this.disabled}
			>
				${this.textStart}
			</button>
		`;
	}
	_renderStarted() {
		return html`
			<button
				@click=${() => this.stop()}
				part="button-stop"
				title="Stop scanning"
			>
				${this.textStop}
			</button>
			<video
				${ref(this.videoRef)}
				autoplay
				part="video-control"
				title=${"Display QR-code(s) to the camera for scanning"}
				@ready=${this._onVideoReady}
			></video>
			${this.mediaDevices.length > 1 ? this._renderDeviceSelect() : null}
		`;
	}
	_renderDeviceSelect() {
		return html`
			<select @input=${this._onDeviceSelect} part="select-device">
				<option value="default" readonly selected>…</option>
				${this.mediaDevices.map((m, i) => this._renderDeviceOption(m, i))}
			</select>
		`;
	}
	_renderDeviceOption(media, index) {
		return html`
			<option value=${media.deviceId}>
				${media.label || `Video ${index + 1}`}
			</option>
		`;
	}

	async stop() {
		if (this.scanInterval) {
			await this._scanIntervalStop(this.scanInterval);
		}
		if (this.userMedia) {
			await this._ungetUserMedia(this.userMedia);
			this.userMedia = null;
		}
	}

	async start() {
		if (this.mediaDevices.length) {
			if (!this.userMedia) {
				this.userMedia = await this._getUserMedia();
			}
		}
		if (!this.userMedia && !this.mediaDevices.length) {
			return this.setAttribute("disabled", true);
		}

		this.detector = await this._getBarcodeDetector();
		if (!this.detector) {
			return this.setAttribute("disabled", true);
		}

		this._startUserMediaVideo();

		if (this.scanInterval) {
			this._scanIntervalStop(this.scanInterval);
		}

		this.scanInterval = this._scanIntervalStart({
			$source: this.videoRef.value,
			detector: this.detector,
			callback: this._onScanSuccess.bind(this),
			interval: this.interval,
		});
	}

	async _onScanSuccess(scanData) {
		const successEvent = new CustomEvent("success", {
			bubbles: false,
			detail: scanData,
		});
		this.dispatchEvent(successEvent);
	}
	async _onDeviceSelect({ target }) {
		this.stop();
		const { value: deviceId } = target;
		this.userMedia = await this._getUserMedia(deviceId);
		if (this.userMedia) {
			await this.start();
		}
	}
	/* always return a usr media if one available;
	 some user medias can be virtual one, such OBS and screen records,
	 and might not be providing a feed currently, but still visible,
	 and marked as available by the browser/device */
	async _getUserMedia(deviceId) {
		let userMedia;
		if (deviceId) {
			try {
				userMedia = await navigator.mediaDevices.getUserMedia({
					...this.constraint,
					video: {
						deviceId,
					},
				});
			} catch (e) {}
		}
		if (!userMedia && this.mediaDevices.length) {
			try {
				userMedia = await navigator.mediaDevices.getUserMedia({
					...this.constraint,
				});
			} catch (e) {}
		}
		return userMedia;
	}
	async _ungetUserMedia(stream) {
		try {
			const track = stream.getTracks()[0];
			if (track) {
				await track.stop();
			}
		} catch (e) {
			console.error("error stoping user media stream", e);
		}
	}

	async _getBarcodeDetector() {
		let detector;
		const supportedFormats = await BarcodeDetector.getSupportedFormats();
		const matchingFormats = this.formats.reduce((formats, formatReq) => {
			if (supportedFormats.indexOf(formatReq) > -1) {
				return [...formats, formatReq];
			} else {
				return formats;
			}
		}, []);

		if (matchingFormats.length) {
			detector = new BarcodeDetector({ formats: matchingFormats });
		}
		return detector;
	}

	_onVideoReady() {
		this._startUserMediaVideo();
	}

	async _startUserMediaVideo() {
		const $video = this.videoRef.value;
		$video.srcObject = this.userMedia;
	}

	_scanIntervalStart({ $source, detector, callback, interval }) {
		this.setAttribute("scanning", "");
		return window.setInterval(async () => {
			const barcodes = await detector.detect($source);
			if (barcodes.length <= 0) return;
			callback(barcodes);
		}, interval || 1000);
	}

	async _scanIntervalStop(scanInterval) {
		return new Promise((resolve, reject) => {
			try {
				clearInterval(scanInterval);
				this.scanInterval = null;
				this.removeAttribute("scanning");
				resolve();
			} catch (error) {
				reject(error);
			}
		});
	}
}
