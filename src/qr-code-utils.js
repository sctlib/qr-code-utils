import './styles/demo.css'

import QRCodeScanner from './qr-code-scanner.js'
import QRCode from './qr-code.js'
import QRCodes from './qr-codes.js'

window.customElements.define('qr-code-scanner', QRCodeScanner)
window.customElements.define('qr-code', QRCode)
window.customElements.define('qr-codes', QRCodes)

export default {
	QRCodeScanner,
	QRCode,
	QRCodes,
}
