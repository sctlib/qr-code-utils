import QRCodeWeb from 'webcomponent-qr-code/qr-code'

/**
 * A qr-code wrapper
 *
 * @csspart svg - the qr-code format svg
 * @csspart img - the qr-code format image
 * @csspart table - the qr-code format httml table
 */
export default class QRCode extends QRCodeWeb {
	get fullscreen() {
		return this.getAttribute('fullscreen') === 'true'
	}

	connectedCallback() {
		if (this.fullscreen) {
			this.addEventListener('click', this._onClick.bind(this))
		}
	}

	disconnectedCallback() {
		if (this.fullscreen) {
			this.removeEventListener('click', this._onClick.bind(this))
		}
	}

	_onClick() {
		if (document.fullscreenElement) {
			document.exitFullscreen()
		} else {
			this.requestFullscreen()
		}
	}
}
