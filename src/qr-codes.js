import { LitElement, html } from 'lit'

/**
 * Display multiple QR-codes, can be animated for multi-scan
 *
 * @csspart qr-code - The qr-code currently displayed
 * @csspart qr-code-svg - The qr-code part="svg"
 * @csspart qr-code-img - The qr-code part="img"
 * @csspart qr-code-table - The qr-code part="table"
 */
export default class QRCodes extends LitElement {
	static get properties() {
		return {
			interval: { type: Number },
			index: { type: Number },
			data: { type: Array, state: true, reflect: true },
			format: { type: String, },
			modulesize: { type: Number },
			margin: { type: Number },
			fullscreen: {type: Boolean },
		}
	}

	constructor() {
		super()
		this.index = 0
		this.interval = 2000
		this.data = JSON.parse(this.getAttribute('data')) || []
		this.format = 'png'
		this.modulesize = 5
		this.margin = 4
	}

	connectedCallback() {
		super.connectedCallback()
		this.rotateInterval = this._rotateIntervalStart()
		if (this.fullscreen) {
			this.addEventListener('click', this._onClick.bind(this))
		}
	}

	disconnectedCallback() {
		super.disconnectedCallback()
		if (this.rotateInterval && this.data) {
			this._rotateIntervalStop(this.rotateInterval)
		}
		if (this.fullscreen) {
			this.removeEventListener('click', this._onClick.bind(this))
		}
	}

	render() {
		return html`
			<qr-code
				data=${this.data[this.index]}
				format=${this.format}
				modulesize=${this.modulesize}
				margin=${this.margin}
				part="qr-code"
				exportparts="svg: qr-code-svg, img: qr-code-img, table: qr-code-table"
				></qr-code>`
	}

	_rotate() {
		this.index = (this.index + 1) % this.data.length
	}

	_rotateIntervalStart() {
		return window.setInterval(this._rotate.bind(this), this.interval || 1000)
	}

	_rotateIntervalStop(windowInterval) {
		return new Promise((resolve, reject) => {
			try {
				window.clearInterval(windowInterval)
				resolve()
			} catch (error) {
				reject(error)
			}
		})
	}

	_onClick() {
		if (document.fullscreenElement) {
			document.exitFullscreen()
		} else {
			this.requestFullscreen()
		}
	}
}
