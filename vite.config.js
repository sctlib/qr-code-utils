// vite.config.js
import { resolve } from "path";
import { defineConfig } from "vite";

/* For this ZBAR bar code scanner lgpg dependency
	 - https://gitlab.com/sctlib/rtc/-/issues/1
	 - https://github.com/undecaf/barcode-detector-polyfill#bundling
	 - https://github.com/undecaf/barcode-detector-polyfill/blob/master/example/rollup.config.js
   */
/* import '@undecaf/barcode-detector-polyfill' // used in rtc-reader */
import replace from "@rollup/plugin-replace";
import { nodeResolve } from "@rollup/plugin-node-resolve";
import terser from "@rollup/plugin-terser";
import { ZBAR_WASM_REPOSITORY } from "@undecaf/barcode-detector-polyfill/zbar-wasm";

// Uncomment this to load module @undecaf/zbar-wasm from an alternate repository, e.g. from https://unpkg.com/
// import { ZBAR_WASM_PKG_NAME, ZBAR_WASM_VERSION } from '@undecaf/barcode-detector-polyfill/zbar-wasm'
// const ALTERNATE_ZBAR_WASM_REPOSITORY = `https://unpkg.com/${ZBAR_WASM_PKG_NAME}@${ZBAR_WASM_VERSION}`

const zbarWasmRepository =
	typeof ALTERNATE_ZBAR_WASM_REPOSITORY !== "undefined"
		? ALTERNATE_ZBAR_WASM_REPOSITORY
		: ZBAR_WASM_REPOSITORY;

export default defineConfig({
	base: "./",
	build: {
		/* minify: true, */
		/* outDir: ".", */
		lib: {
			// Could also be a dictionary or array of multiple entry points
			entry: resolve(__dirname, "src/index.js"),
			formats: ["es"],
			name: "rtcpeer",
			// the proper extensions will be added
			fileName: "qr-code-utils",
		},
		rollupOptions: {
			/* out input file to bundle the js & css */
			input: {
				main: resolve(__dirname, "index.html"),
			},

			/* declare zbar as a dependency, to NOT-bundle,
			 but load from CDN (lgpl see issue#1) */
			external: [
				`${zbarWasmRepository}/dist/main.js`,
				/* /^lit/, */
			],
			plugins: [
				nodeResolve(),
				replace({
					values: {
						/* Replaces the repository URL,
							 with the alternate repository URL if necessary */
						[ZBAR_WASM_REPOSITORY]: zbarWasmRepository,
					},
					preventAssignment: true,
				}),
				terser(),
			],
		},
	},
});
