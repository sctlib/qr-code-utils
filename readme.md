# qr-code-utils

A library of web-components and tools to display and scan QR-codes and barcodes on the web.

- [npm](https://www.npmjs.com/package/@sctlib/qr-code-utils)
- usage and style [examples](https://sctlib.gitlab.io/qr-code-utils)

## `qr-code`

Sources:

- https://github.com/educastellano/qr-code (MIT)

### Attributes

- all the original attributes (see doc above)
- `fullscreen="true"`, to "request fullscreen for the element, on click. Clicking again will exit fullscreen

### Parts

There is one part for each different `format` of the qr-code, on their respective DOM element.

- `svg` on the `<svg/>` element, for the format `svg`
- `img` on the `<img/>` element, for the image `format`
- `table` on the `<table/>` element, for the format `html`

## `qr-codes`

A component to display multiple qr-code sequencially, in an infinite loop. The idea is to be able to use the `qr-code-scanner` on the other end, to continuously scan through a loop of information displayed in the `qr-codes`, until it has been received.

## Attributes

- all `qr-code` attributes, which will be passed directly (including `fullscreen`)
- `interval`, the duration in milliseconds for which each qr-code will be displayed; it will default to `2000`.
- `data` is an Array of `qr-code[data]` attribute

### Parts

- `qr-code` on the `<qr-code/>` children element
- `qr-code-svg` on the qr-code's children `svg` part
- `qr-code-img` on the qr-code's children `img` part
- `qr-code-table` on the qr-code's children `table` part

## `qr-code-scanner`

This component will display a qr-code-scanner with a start and stop button, and a video element used for human visual controling.

> The scanner can scan multiple types of barcodes, not only QR codes.

Sources:

- https://github.com/undecaf/barcode-detector-polyfill (MIT)
- https://github.com/undecaf/zbar-wasm (LGPL-2.1+)
- https://developer.mozilla.org/en-US/docs/Web/API/Barcode_Detection_API

### Attributes

- `text-start`, the inner text of the `button-start`
- `text-stop`, the inner text of the `button-stop`
- `interval`, the interval in milliseconds between each scanning attempt. Will default to `1000`
- `formats`, an array of the formats the scanning will look for. Will default to `['qr_code']`, and is compared again the device supported reading formats
- `scanning`, present if currently scanning
- `disabled`, present if the scanner cannot be used on the current device, or the user rejected the authorizations
- `autoscan`, default is `false`, will request camera permissions to start scanning when the element is inserted in the DOM

### Events

- @`success`, happens when scanning found one or multiple QR code. The
  event detail is an Array of scanned barcodes, as the scanner can
  scan multiple barcodes in one image/frame.

### Parts

To style the shadow element's children, these parts are available:

- `button-start`,the `<button/>` that request permission to access the camera
- `button-stop`, the `<button/>` that stops the camera from recording
- `video-control`, the `<video/>` used to control scanning & qr-code position
- `select-device`, the `<select/>` element if multiple `videoinput` media devices are available to the browser
